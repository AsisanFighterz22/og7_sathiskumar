package Lerneintrag;

import java.io.*;

public class FileControl {
	public static void main(String[] args) {
		File f = new File("C:\\Users\\user\\Desktop\\miriam.dat");
		dateiEinlesenUndAusgeben(f);
	}

	public static void dateiEinlesenUndAusgeben(File f) {
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			while (s != null) {
				System.out.println(s);
				s = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
