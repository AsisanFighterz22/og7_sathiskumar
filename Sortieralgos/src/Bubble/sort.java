package Bubble;

public class sort {

	public static void main(String[] args) {
		System.out.println("Liste = (88,46,135,13467,223469,225,564976,2310,3369465,223164,,134679,2648,1304679813,123456,316479,320146,2587643,134670164,33,22,45,98)");
		int[] unsortiert = {88,46,135,13467,223469,225,564976,2310,3369465,223164,134679,2648,1304679813,123456,316479,320146,2587643,134670164,33,22,45,98};
		int[] sortiert = bubblesort(unsortiert);

		for (int i = 0; i < sortiert.length-1; i++) {
			System.out.print(sortiert[i] + ", ");
		}
		for (int i = sortiert.length-1; i < sortiert.length; i++) {
			System.out.print(sortiert[i]);
		}
	}

	public static int[] bubblesort(int[] zahl) {
		int temp;
		for (int i = 0; i < zahl.length; i++) {
			for (int j = 0; j < zahl.length - 1; j++) {
				if (zahl[j] > zahl[j+1]) {
					temp = zahl[j];
					zahl[j] = zahl[j + 1];
					zahl[j + 1] = temp;
				}
			}
		}
		return zahl;
	}
}
