package Interpolation;

public class Interpolationssuche {
	 public int interpolierteSuche(int schluessel, int daten[]) {
		    int von = 0;                  // linke Teilfeldbegrenzung
		    int bis = daten.length - 1;  // rechte Teilfeldbegrenzung
		    int aktpos;                     // Anzahl verschiedener Elemente
		    int pos;                        // aktuelle Teilungsposition

		    // solange der Schl�ssel im Bereich liegt (andernfalls ist das gesuchte
		    // Element nicht vorhanden)
		    while( schluessel >= daten[von] && schluessel <= daten[bis] ){
		    	
		      // Berechnung der neuen interpolierten Teilungsposition
		      pos = von + ((bis - von) * (schluessel - daten[von]/daten[bis] - daten[von]));

		      if( schluessel > daten[pos] )            // rechtes Teilintervall
		        von = pos + 1;                       // daten[pos] bereits �berpr�ft
		      else if( schluessel < daten[pos] )      // linkes Teilintervall
		        bis = pos - 1;                      // daten[pos] bereits �berpr�ft
		      else                                     // Element gefunden
		        return pos;                            // Position zur�ckgeben
		    }

		    return -1;                                 // Element nicht gefunden
		  }
}
