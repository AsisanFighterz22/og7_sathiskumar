package GridLayout;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class TicTacToe extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe frame = new TicTacToe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicTacToe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));

		for (int i = 0; i < 9; i++) {
			JButton btn = new JButton("");
			btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					changeState(btn);
				}
			});
			contentPane.add(btn);
		}

	}

	public void changeState(JButton btn) {
		if (btn.getText().equals("")) {
			btn.setText("X");
		} else if (btn.getText().equals("X")) {
			btn.setText("O");
		} else {
			btn.setText("");
		}
	}
}
