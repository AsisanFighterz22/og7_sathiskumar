package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet {

private double posX;
private double posY;
private int anzahlHafen;
private String name;

	public Planet(double posX, double posY, int anzahlHafen, String name) {
	super();
	this.posX = posX;
	this.posY = posY;
	this.anzahlHafen = anzahlHafen;
	this.name = name;
}

	public double getPosX() {
		if (meinPlanet.getPosX() == posX)
			System.out.println("Implementierung 'Position X' korrekt!");
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		if (meinPlanet.getPosY() == posY)
			System.out.println("Implementierung 'Position Y' korrekt!");
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getAnzahlHafen() {
		if (meinPlanet.getAnzahlHafen() == anzahlHafen)
			System.out.println("Implementierung 'Hafen' korrekt!");
		return anzahlHafen;
	}

	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}

	public String getName() {
		if (meinPlanet.getName().equals(name))
			System.out.println("Implementierung 'Name'  korrekt!");
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
