package de.oszimt.starsim2099;

public class Pilot {
private double posX;
private double posY;
private String grad;
private String name;
public Pilot(double posX, double posY, String grad, String name) {
	super();
	this.posX = posX;
	this.posY = posY;
	this.grad = grad;
	this.name = name;
}
public double getPosX() {
	return posX;
}
public void setPosX(double posX) {
	this.posX = posX;
}
public double getPosY() {
	return posY;
}
public void setPosY(double posY) {
	this.posY = posY;
}
public String getGrad() {
	return grad;
}
public void setGrad(String grad) {
	this.grad = grad;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}