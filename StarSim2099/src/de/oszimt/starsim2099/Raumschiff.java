package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

private double posX;
private double posY;
private int maxLadeKapazitaet;
private String typ;
private String antrieb;
private int winkel;
	
	public Raumschiff(double posX, double posY, int maxLadeKapazitaet, String typ, String antrieb, int winkel) {
	super();
	this.posX = posX;
	this.posY = posY;
	this.maxLadeKapazitaet = maxLadeKapazitaet;
	this.typ = typ;
	this.antrieb = antrieb;
	this.winkel = winkel;
}
	
	public double getPosX() {
		if (meinRaumschiff.getPosX() == posX)
			System.out.println("Implementierung 'Position X' korrekt!");
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		if (meinRaumschiff.getPosY() == posY)
			System.out.println("Implementierung 'Position Y' korrekt!");
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMaxLadeKapazitaet() {
		if (meinRaumschiff.getMaxLadekapazitaet() == maxKapazitaet)
			System.out.println("Implementierung 'Kapazität' korrekt!");
		return maxKapazitaet;
	}

	public void setMaxLadeKapazitaet(int maxKapazitaet) {
		this.maxLadeKapazitaet = maxLadeKapazitaet;
	}

	public String getTyp() {
		if (meinRaumschiff.getTyp().equals(typ))
			System.out.println("Implementierung 'Typ' korrekt!");
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		if (meinRaumschiff.getAntrieb().equals(antrieb))
			System.out.println("Implementierung 'Antrieb' korrekt!");
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getWinkel() {
		if (meinRaumschiff.getWinkel() == winkel)
			System.out.println("Implementierung 'Winkel' korrekt!");
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
