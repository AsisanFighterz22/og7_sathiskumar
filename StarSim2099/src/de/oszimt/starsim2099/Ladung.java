package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {
private double posX;
private double posY;
private int masse;
private String typ;

	public Ladung(double posX, double posY, int masse, String typ) {
	super();
	this.posX = posX;
	this.posY = posY;
	this.masse = masse;
	this.typ = typ;
}

	public double getPosX() {
		
		if (meineLadung.getPosX() == posX)
			System.out.println("Implementierung 'Position X' korrekt!");
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		if (meineLadung.getPosY() == posY)
			System.out.println("Implementierung 'Position Y' korrekt!");
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMasse() {
		if (meineLadung.getMasse() == masse)
			System.out.println("Implementierung 'Masse'  korrekt!");
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public String getTyp() {
		if (meineLadung.getTyp().equals(typ))
			System.out.println("Implementierung 'Typ'  korrekt!");
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}