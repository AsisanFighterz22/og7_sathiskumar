package Collection;

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 6) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
                myScanner.nextLine();
				System.out.println("Geben sie den Autor ein:");
                String autor = myScanner.nextLine();
                System.out.println("Geben sie den Titel ein:");
                String titel = myScanner.nextLine();
                System.out.println("Geben sie die ISBN ein:");
                String isbn = myScanner.nextLine();
                Buch neuBuch = new Buch(autor,titel,isbn); 
				buchliste.add(neuBuch);
                break;
			case '2':
				System.out.println("Bitte geben sie die gesuchte ISBN ein:");
				isbn = myScanner.next();
				if (findeBuch(buchliste, isbn) != null) {
				neuBuch = findeBuch(buchliste,isbn);
				System.out.println(neuBuch.toString());
				}
				else {
					System.err.println("\t\t Buch wurde nicht gefunden !!!");
					}
				break;
			case '3':
				System.out.println("Bitte geben sie die gesuchte ISBN ein:");
				isbn = myScanner.next();
				if (findeBuch(buchliste, isbn) != null) {
				neuBuch = findeBuch(buchliste, isbn);
				loescheBuch(buchliste, neuBuch);
				}
				else {
				System.err.println("\t\t Buch wurde nicht gefunden !!!");
				}
				break;
			case '4':
				System.out.println("Die gr��te ISBN ist: " + ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.out.println("Bitte geben sie die gesuchte ISBN ein:");
				isbn = myScanner.next();
				if (findeBuch(buchliste, isbn) != null) {
				neuBuch = findeBuch(buchliste,isbn);
				System.out.println(neuBuch.toString());
				}
				else {
					System.err.println("\t\t Buch wurde nicht gefunden !!!");
					System.out.println("\t\t Bitte geben sie dei neuen Dtaen beim Sekretariat ein");
					}
				break;
			case '6':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 6);
}// main
	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).equals(new Buch("", "", isbn))) {
				return buchliste.get(i);
			}
		}
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, Buch neuBuch) {
		return buchliste.remove(neuBuch);
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		Buch aBuch = new Buch();
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).compareTo(aBuch) > 0) {
				aBuch = buchliste.get(i);
			}
		}
		return aBuch.getIsbn();
	}
}
