package Karte;

public class Held {
	// Anfang Attribute
	private String name;
	private String typ;
	private int maxLeben;
	private int aktLeben;
	private int ruestung;
	private int magieresistenz;
	private int angriff;
	private String bildpfad;

	// private String nameAngriff;
	// Ende Attribute
	public Held(String name, String typ, int maxLeben, int aktLeben, int ruestung, int magieresistenz, int angriff, int bildpfad) {
		super();
		this.name = "";
		this.typ = "";
		this.maxLeben = 0;
		this.aktLeben = 0;
		this.ruestung = 0;
		this.magieresistenz = 0;
		this.angriff = 0;
		this.bildpfad = null;
	}

	public Held(String name, String typ, int maxLeben, int aktLeben, int ruestung, int magieresistenz, int angriff,	String bildpfad) {
		super();
		this.name = name;
		this.typ = typ;
		this.maxLeben = maxLeben;
		this.aktLeben = aktLeben;
		this.ruestung = ruestung;
		this.magieresistenz = magieresistenz;
		this.angriff = angriff;
		this.bildpfad = "./src/bilder/" + this.name + ".jpg";
	}

	// Anfang der Getter & Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getMaxLeben() {
		return maxLeben;
	}

	public void setMaxLeben(int maxLeben) {
		this.maxLeben = maxLeben;
	}

	public int getAktLeben() {
		return aktLeben;
	}

	public void setAktLeben(int aktLeben) {
		this.aktLeben = aktLeben;
	}

	public int getRuestung() {
		return ruestung;
	}

	public void setRuestung(int ruestung) {
		this.ruestung = ruestung;
	}

	public int getMagieresistenz() {
		return magieresistenz;
	}

	public void setMagieresistenz(int magieresistenz) {
		this.magieresistenz = magieresistenz;
	}

	public int getAngriff() {
		return angriff;
	}

	public void setAngriff(int angriff) {
		this.angriff = angriff;
	}

	public String getBildpfad() {
		return bildpfad;
	}

	public void setBildpfad(String bildpfad) {
		this.bildpfad = bildpfad;
	}
	// Ende der Getter & Setter

	//Anfang der Methoden
	public int angreifen() {
		return this.angriff;
	}
	
	public void leiden(int schaden) {
		if (schaden - this.ruestung <= 0) {
			aktLeben -= 1;
		}
		else {
			aktLeben -= schaden - this.ruestung;
		}
	}
	
	public void heilen() {
		this.aktLeben = this.maxLeben;
	}
}
