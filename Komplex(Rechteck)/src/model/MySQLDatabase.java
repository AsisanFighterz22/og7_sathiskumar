package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class MySQLDatabase {

	private String driver = "com.mysql.jbdc.Driver";
	private String url = "jbdc:mysql://localhorst/rechtecke?";
	private String user = "root";
	private String password = "";
	
	
	public void rechteck_eintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Rechtecke (x,y,Breite,Hoehe) VALUES" + "(?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

   public List<Rechteck> getAllleRechtecke() {
	   List rechtecke = new LinkedList<Rechteck>();
	   try {
		   Class.forName(driver);
		   Connection con = DriverManager.getConnection(url, user, password);
		   String sql = "SELECT * FROM T_Rechtecke";
		   Statement s = con.createStatement();
		   ResultSet rs = s.executeQuery(sql);
		   while(rs.next()) {
			   	int x = rs.getInt("X");
			   	int y = rs.getInt("Y");
			   	int Breite =rs.getInt("Breite"); 
			   	int Hoehe =rs.getInt("Hoehe");
			   	rechtecke.add(new Rechteck(x, y, Breite, Hoehe));
		   }
		   
		   con.close();
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   return rechtecke;
   }
}
