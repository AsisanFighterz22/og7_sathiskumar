package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.BunteRechteckeGUI;

public class BunteRechteckeController {
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAllleRechtecke();
		
	}
	
	public List getRechtecke(){
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteck_eintragen(rechteck);
		
	}
	
	public void reset() {
		rechtecke.clear();
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckhinzufügen() {
		new BunteRechteckeGUI(this); 
	}
}
