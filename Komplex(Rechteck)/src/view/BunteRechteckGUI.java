package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;

public class BunteRechteckGUI extends JFrame {

	private JPanel contentPane;
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuItemneuesRechteck;
	private BunteRechteckeController brc;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	BunteRechteckGUI frame = new BunteRechteckGUI();
	frame.run();			
	}

	protected void run() {
		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
		
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGUI() {
		this.brc = new BunteRechteckeController();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1980, 1024);
		contentPane = new Zeichenflaeche(this.brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.menubar = new JMenuBar();
		setJMenuBar(menubar);
		this.menu = new JMenu("Hinzufügen");
		this.menubar.add(menu);
		this.menuItemneuesRechteck = new JMenuItem("Rechteck hinzufügen");
		this.menu.add(this.menuItemneuesRechteck);
		this.menuItemneuesRechteck.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuItemneuesRechteck_Clicked();
				
			}
		});
		setVisible(true);
	}

	protected void menuItemneuesRechteck_Clicked() {
		this.brc.rechteckhinzufügen();
		
	}

}
