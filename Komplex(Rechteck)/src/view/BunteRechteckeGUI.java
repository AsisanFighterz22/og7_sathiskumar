package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.glass.events.MouseEvent;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;

import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class BunteRechteckeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblHhe;
	private JLabel lblBreite;
	private BunteRechteckeController ctr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BunteRechteckeGUI frame = new BunteRechteckeGUI(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckeGUI(BunteRechteckeController brc) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblX = new JLabel("X");
		contentPane.add(lblX);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblY = new JLabel("Y");
		contentPane.add(lblY);
		
		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		lblHhe = new JLabel("H\u00F6he");
		contentPane.add(lblHhe);
		
		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		lblBreite = new JLabel("Breite");
		contentPane.add(lblBreite);
		
		textField_3 = new JTextField();
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
	this.ctr = brc;	
		
		JButton btnhinzufuegen = new JButton("Hinzufügen");
		btnhinzufuegen.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {		
			int x = Integer.parseInt(textField.getText());
			int y = Integer.parseInt(textField_1.getText());
			int Hoehe = Integer.parseInt(textField_2.getText());
			int Breite = Integer.parseInt(textField_3.getText());
			Rechteck neu = new Rechteck(x, y, Breite, Hoehe);
			ctr.add(neu);
		}	
	});	
	contentPane.add(btnhinzufuegen);		
	setVisible(true); 
	}
}
