
public class Trainer extends Personalien{
private char lizenzklasee;
private double monAufwandsEntschaedigung;

public Trainer(){
}

public Trainer(char lizenzklasee, double monAufwandsEntschaedigung) {
	super();
	this.lizenzklasee = lizenzklasee;
	this.monAufwandsEntschaedigung = monAufwandsEntschaedigung;
}

public char getLizenzklasee() {
	return lizenzklasee;
}

public void setLizenzklasee(char lizenzklasee) {
	this.lizenzklasee = lizenzklasee;
}

public double getMonAufwandsEntschaedigung() {
	return monAufwandsEntschaedigung;
}

public void setMonAufwandsEntschaedigung(double monAufwandsEntschaedigung) {
	this.monAufwandsEntschaedigung = monAufwandsEntschaedigung;
}

}
