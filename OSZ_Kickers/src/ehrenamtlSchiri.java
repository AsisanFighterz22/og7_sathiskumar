
public class ehrenamtlSchiri extends Personalien {
private int anzGepfiffeneSpiele;

public ehrenamtlSchiri() {
}

public ehrenamtlSchiri(int anzGepfiffeneSpiele) {
	super();
	this.anzGepfiffeneSpiele = anzGepfiffeneSpiele;
}

public int getAnzGepfiffeneSpiele() {
	return anzGepfiffeneSpiele;
}

public void setAnzGepfiffeneSpiele(int anzGepfiffeneSpiele) {
	this.anzGepfiffeneSpiele = anzGepfiffeneSpiele;
}

}
