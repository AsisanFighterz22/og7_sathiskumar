public class Mannschaftsleiter extends Personalien {
private String mannschaftsname;
private boolean zusatzaufgabe;

public Mannschaftsleiter() {
}

public Mannschaftsleiter(String mannschaftsname, boolean zusatzaufgabe) {
	super();
	this.mannschaftsname = mannschaftsname;
	this.zusatzaufgabe = zusatzaufgabe;
}

public String getMannschaftsname() {
	return mannschaftsname;
}

public void setMannschaftsname(String mannschaftsname) {
	this.mannschaftsname = mannschaftsname;
}

public boolean isZusatzaufgabe() {
	return zusatzaufgabe;
}

public void setZusatzaufgabe(boolean zusatzaufgabe) {
	this.zusatzaufgabe = zusatzaufgabe;
}

}
