
public class Personalien {
private String name;
private long telNr;
private boolean jahresbetrag;

public Personalien() {
}

public Personalien(String name, long telNr, boolean jahresbetrag) {
	super();
	this.name = name;
	this.telNr = telNr;
	this.jahresbetrag = jahresbetrag;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public long getTelNr() {
	return telNr;
}

public void setTelNr(long telNr) {
	this.telNr = telNr;
}

public boolean isjahresbetrag() {
	return jahresbetrag;
}

public void setJahresbetrag(boolean jahresbetrag) {
	this.jahresbetrag = jahresbetrag;
}

}