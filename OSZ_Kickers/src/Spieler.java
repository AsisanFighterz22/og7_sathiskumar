
public class Spieler extends Personalien {
private int trikotNr;
private String position;

public Spieler() {
}

public Spieler(int trikotNr, String position) {
	super();
	this.trikotNr = trikotNr;
	this.position = position;
}

public int getTrikotNr() {
	return trikotNr;
}

public void setTrikotNr(int trikotNr) {
	this.trikotNr = trikotNr;
}

public String getPosition() {
	return position;
}

public void setPosition(String position) {
	this.position = position;
}
}