package Uhr;

public class Stoppuhr {
private long start;
private long stopp;

public Stoppuhr() {
	super();
}

public Stoppuhr(long start, long stopp) {
	super();
	this.start = start;
	this.stopp = stopp;
}

public void start() {
this.start = System.currentTimeMillis();	
}

public void stopp() {
this.stopp = System.currentTimeMillis();
}

public void reset() {
this.start = 0;
this.stopp = 0;
}

public long getMs() {
long Ms = stopp - start;	
	return Ms;	
	}
}
